package com.company;

import com.company.sorts.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            printOptions();
            System.out.println("Enter choice:");
            String choice = scanner.nextLine();
            switch (choice) {
                case "1" -> new Sort();
                case "2" -> new SortSplit();
                case "3" -> new SortThread();
                case "4" -> new SortSplitThread();
                case "5" -> new SortThreads();
                default -> System.out.println("Unknown command.");
            }
        }
    }

    public static void printOptions() {
        System.out.println("---------------Concurrency--------------");
        System.out.println("- 1. Standard sort                     -");
        System.out.println("- 2. Sort with split array             -");
        System.out.println("- 3. Sort with Thread                  -");
        System.out.println("- 4. Sort with split array and Threads -");
        System.out.println("- 5. Threads optimization              -");
        System.out.println("----------------------------------------");
    }
}
