package com.company.sorts;

import com.company.Methods;

import java.util.concurrent.TimeUnit;

public class Sort {

    public Sort() {
        int[] numbers = {25_000, 50_000, 100_000, 200_000, 400_000};
        int number;
        for (int i = 0; i < numbers.length; i++) {
            number = numbers[i];
            System.out.println("Measuring: " + number);
            for (int j = 0; j < 6; j++) {
                int[] arr = Methods.provideArray(number);

                long start = System.nanoTime();

                Methods.sort(arr);

                System.out.println(TimeUnit.MILLISECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS));
            }
        }
    }
}
