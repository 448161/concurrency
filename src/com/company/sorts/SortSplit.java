package com.company.sorts;

import com.company.Methods;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class SortSplit {

    public SortSplit() {
        int[] numbers = {25_000, 50_000, 100_000, 200_000, 400_000};
        int number;
        for (int i = 0; i < numbers.length; i++) {
            number = numbers[i];
            System.out.println("Measuring: " + number);
            for (int j = 0; j < 6; j++) {
                int[] arr = Methods.provideArray(number);

                long start = System.nanoTime();

                int[] arr_1 = Arrays.copyOfRange(arr, 0, arr.length/2);
                int[] arr_2 = Arrays.copyOfRange(arr, arr.length/2, arr.length);

                Methods.sort(arr_1);
                Methods.sort(arr_2);

                arr = Methods.merge(arr_1, arr_2);

                System.out.println(TimeUnit.MILLISECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS));
            }
        }
    }


}
