package com.company.sorts;

import com.company.Methods;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class SortThreads {

    public SortThreads() {
            int[] numbers = {25_000, 50_000, 100_000, 200_000, 400_000};
            int number;
            for (int i = 0; i < numbers.length; i++) {
                number = numbers[i];
                System.out.println("Measuring: " + number);
                for (int j = 0; j < 6; j++) {
                    Integer[] arr = provideArray(number);
                    List<Integer> ints = asList(arr);

                    long start = System.nanoTime();

                    ForkJoinPool pool = new ForkJoinPool();
                    ForkJoinTask<List<Integer>> task = new MergeTask(ints);
                    List<Integer> result = pool.invoke(task);

                    System.out.println(TimeUnit.MILLISECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS));
                }
            }
    }

        public class MergeTask extends RecursiveTask<List<Integer>> {
            private static final int THRESHOLD = 4;
            private final List<Integer> list;

            public MergeTask(List<Integer> list) {
                this.list = list;
            }

            @Override
            protected List<Integer> compute() {
                if (list.size() < THRESHOLD) {
                    return sort(list);
                }

                MergeTask left = new MergeTask(list.stream().limit(list.size()/2).collect(toList()));
                MergeTask right = new MergeTask(list.stream().skip(list.size()/2).collect(toList()));
                invokeAll(left, right);

                return merge(left.join(), right.join());
            }
        }

        public List<Integer> sort(List<Integer> list) {
            int n = list.size();
            for (int i = 1; i < n; ++i) {
                int key = list.get(i);
                int j = i - 1;

                while (j >= 0 && list.get(j) > key) {
                    list.set(j + 1, list.get(j));
                    j = j - 1;
                }
                list.set(j + 1, key);
            }
            return list;
        }

        public List<Integer> merge(List<Integer> a, List<Integer> b) {
            int i=0, j=0;
            List<Integer> result = new ArrayList<>(a.size() + b.size());
            while(i < a.size() && j < b.size())
                result.add(a.get(i) < b.get(j) ? a.get(i++): b.get(j++));

            while(i < a.size())
                result.add(a.get(i++));

            while(j < b.size())
                result.add(b.get(j++));

            return result;
        }

    public static Integer[] provideArray(int length) {
        Integer[] array = new Integer[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int)(Math.random() * 100);
        }
        return array;
    }
}
